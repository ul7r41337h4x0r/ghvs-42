/*

    GHVS-42 Copyright (C) 2018  scott snyder

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package main

import (
	"fmt"
	"net/http"
	"os"
	"strings"
)

var fileloc string

func browseS(w http.ResponseWriter, r *http.Request) {
	var serieslist [1000]string
	for xr := 0; xr < len(serieslist); xr++ {
		serieslist[xr] = ""
	}
	fmt.Fprint(w, `<!DOCTYPE html>
<html lang='en'>
    <head>
        <meta charset='UTF-8'>
        <title>Browse</title>
    </head>
    <body bgcolor="black">
`)
	series := r.FormValue("Sid")
	d, _ := os.Open(fileloc + "vid/" + series + "/")
	defer d.Close()
	fi, _ := d.Readdir(-1)
	for x, fi := range fi {
		if fi.Mode().IsRegular() {
			serieslist[x] = fmt.Sprintf("%s", fi.Name())
		} else {
			serieslist[x] = fmt.Sprintf("SUBFOLDER:%s", fi.Name())
		}
	}
	for _, sl := range serieslist {
		if sl == "" {
			break
		}
		sfs := strings.Contains(sl, "SUBFOLDER")
		if sfs {
			spscs := strings.Split(sl, ":")
			fmt.Fprint(w, `        <a href='browseS?Sid=`+series+"/"+spscs[1]+`'>
            <img src='img/`+series+"/"+spscs[1]+`.jpg' width='300' height='400' style=border-width:5px;'>
        </a>
`)
		} else {
			srstr := strings.Split(sl, ".")
			fmt.Fprint(w, `        <a href='videos?vidid=`+series+"/"+srstr[0]+`'>
            <img src='img/`+series+"/"+srstr[0]+`.jpg' width='300' height='400' style=border-width:5px;'>
        </a>
`)
		}
	}
	fmt.Fprint(w, `    </body>
</html>
`)
}

func browse(w http.ResponseWriter, _ *http.Request) {
	var vidlist [1000]string
	d, _ := os.Open(fileloc + "vid/")
	defer d.Close()
	fi, _ := d.Readdir(-1)
	for x, fi := range fi {
		if fi.Mode().IsRegular() {
			vidlist[x] = fmt.Sprintf("%s", fi.Name())
		} else {
			vidlist[x] = fmt.Sprintf("SERIES:%s", fi.Name())
		}
	}
	fmt.Fprint(w, `<!DOCTYPE html>
<html lang='en'>
    <head>
        <meta charset='UTF-8'>
        <title>Browse</title>
    </head>
    <body bgcolor="black">
`)
	for _, vl := range vidlist {
		scs := strings.Contains(vl, "SERIES:")
		if len(vl) < 2 {
			break
		}
		if scs {
			spscs := strings.Split(vl, ":")
			fmt.Fprint(w, `        <a href='browseS?Sid=`+spscs[1]+`'>
            <img src='img/`+spscs[1]+`.jpg' width='300' height='400' style=border-width:5px;'>
        </a>
`)
		} else {
			spstr := strings.Split(vl, ".")
			fmt.Fprint(w, `        <a href='videos?vidid=`+spstr[0]+`'>
            <img src='img/`+spstr[0]+`.jpg' width='300' height='400' style=border-width:5px;'>
        </a>
`)
		}
	}
	fmt.Fprint(w, `    </body>
</html>
`)
}

func vidreturn(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, `<!DOCTYPE html>
<html lang='en'>
    <head>
        <meta charset='UTF-8'>
        <title>`+r.FormValue("vidid")+`</title>
    </head>
    <body bgcolor="black">
        <video width='100%' controls autoplay>
            <source src='vid/`+r.FormValue("vidid")+`.mp4' type='video/mp4'>
            Your browser does not support the video tag.
        </video>
    </body>
</html>
`)
}

func main() {
	if len(os.Args) < 5 {
		fmt.Printf("len args is: %d\nlen args needs to be: 5\n", len(os.Args))
		fmt.Println(`requires 2 args
    -r --root <server root>
    -p --port <server port>`)
		os.Exit(2)
	}
	var serverport string
	for x := 0 ; x < len(os.Args) ; x++ {
		if os.Args[x] == "-r" || os.Args[x] == "--root" {
			fileloc = os.Args[x+1]
			fileLocLen := len(fileloc)
			if fileloc[fileLocLen-1:fileLocLen] != "/" {
				fileloc = fileloc + "/"
			}
		}
		if os.Args[x] == "-p" || os.Args[x] == "--port" {
			serverport = os.Args[x+1]
		}
	}
	http.Handle("/", http.FileServer(http.Dir(fileloc)))
	http.HandleFunc("/browse", browse)
	http.HandleFunc("/browseS", browseS)
	http.HandleFunc("/videos", vidreturn)
	http.ListenAndServe(":"+serverport, nil)
}
